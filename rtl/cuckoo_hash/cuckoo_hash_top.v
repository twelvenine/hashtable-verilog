`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2022/03/19 15:29:28
// Design Name: 
// Module Name: cuckoo_hash_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

(* dont_touch="true" *)

module cuckoo_hash_top#(
    parameter               SN  =   1,      //slot number
                            HW  =   10,     //hash width
                            DW  =   32,     //data width
                            RN  =   1,      //reinsert number
                            RCW =   8       //reinsert counter width
    )(
    //system signals
    input                   clk,
    input                   rst_n,

    //insert
    input                   insert_i,
    input   [DW-1:0]        insert_data_i,
    output                  insert_error_o,
    output                  insert_end_o,

    //delete
    input                   delete_i,
    input   [DW-1:0]        delete_data_i,
    output                  delete_error_o,
    output                  delete_end_o,

    //search a
    input                   search_a_i,
    input   [DW-1:0]        search_a_data_i,
    output                  search_a_exist_o,
    output                  search_a_end_o,

    //search b
    input                   search_b_i,
    input   [DW-1:0]        search_b_data_i,
    output                  search_b_exist_o,
    output                  search_b_end_o
    );

    //RAM A
    wire                    rama_wea;
    wire    [HW-1:0]        rama_addra;
    wire    [(DW+1)*SN-1:0] rama_dina;
    wire    [(DW+1)*SN-1:0] rama_douta;

    wire                    rama_web;
    wire    [HW-1:0]        rama_addrb;
    wire    [(DW+1)*SN-1:0] rama_dinb;
    wire    [(DW+1)*SN-1:0] rama_doutb;

    //RAM B
    wire                    ramb_wea;
    wire    [HW-1:0]        ramb_addra;
    wire    [(DW+1)*SN-1:0] ramb_dina;
    wire    [(DW+1)*SN-1:0] ramb_douta;

    wire                    ramb_web;
    wire    [HW-1:0]        ramb_addrb;
    wire    [(DW+1)*SN-1:0] ramb_dinb;
    wire    [(DW+1)*SN-1:0] ramb_doutb;

    //cuckoo hash
    cuckoo_hash #(
        .SN                 (SN),   //slot number
        .HW                 (HW),   //hash width
        .DW                 (DW),   //data width
        .RN                 (RN),   //reinsert number
        .RCW                (RCW)   //reinsert counter width
    ) U_cuckoo_hash(
        //system signals
        .clk                (clk),
        .rst_n              (rst_n),

        //insert
        .insert_i           (insert_i),
        .insert_data_i      (insert_data_i),
        .insert_error_o     (insert_error_o),
        .insert_end_o       (insert_end_o),

        //delete
        .delete_i           (delete_i),
        .delete_data_i      (delete_data_i),
        .delete_error_o     (delete_error_o),
        .delete_end_o       (delete_end_o),

        //search port-a
        .search_a_i         (search_a_i),
        .search_a_data_i    (search_a_data_i),
        .search_a_exist_o   (search_a_exist_o),
        .search_a_end_o     (search_a_end_o),

        //search port-b
        .search_b_i         (search_b_i),
        .search_b_data_i    (search_b_data_i),
        .search_b_exist_o   (search_b_exist_o),
        .search_b_end_o     (search_b_end_o),

        //RAM A interface
        //side A
        .rama_wea_o         (rama_wea),
        .rama_addra_o       (rama_addra),
        .rama_dina_o        (rama_dina),
        .rama_douta_i       (rama_douta),

        //side B
        .rama_web_o         (rama_web),
        .rama_addrb_o       (rama_addrb),
        .rama_dinb_o        (rama_dinb),
        .rama_doutb_i       (rama_doutb),

        //RAM B interface
        //side A
        .ramb_wea_o         (ramb_wea),
        .ramb_addra_o       (ramb_addra),
        .ramb_dina_o        (ramb_dina),
        .ramb_douta_i       (ramb_douta),

        //side B
        .ramb_web_o         (ramb_web),
        .ramb_addrb_o       (ramb_addrb),
        .ramb_dinb_o        (ramb_dinb),
        .ramb_doutb_i       (ramb_doutb)
    );

    //ram
    cuckoo_hash_ram_top #(
        .SN                 (SN),   //slot number
        .DW                 (DW),   //data width
        .HW                 (HW)    //hash width
    ) U_cuckoo_hash_ram_top(
        //system signals
        .clk                (clk),
        .rst_n              (rst_n),

        //RAM A
        .rama_wea_i         (rama_wea),
        .rama_addra_i       (rama_addra),
        .rama_dina_i        (rama_dina),
        .rama_douta_o       (rama_douta),

        .rama_web_i         (rama_web),
        .rama_addrb_i       (rama_addrb),
        .rama_dinb_i        (rama_dinb),
        .rama_doutb_o       (rama_doutb),

        //RAM B
        .ramb_wea_i         (ramb_wea),
        .ramb_addra_i       (ramb_addra),
        .ramb_dina_i        (ramb_dina),
        .ramb_douta_o       (ramb_douta),

        .ramb_web_i         (ramb_web),
        .ramb_addrb_i       (ramb_addrb),
        .ramb_dinb_i        (ramb_dinb),
        .ramb_doutb_o       (ramb_doutb)
    );
endmodule
