`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2022/03/21 13:03:33
// Design Name: 
// Module Name: cuckoo_hash_ram_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

(* dont_touch="true" *)

module cuckoo_hash_ram_top#(
    parameter               SN  =   1,      //slot number
                            DW  =   32,     //data width
                            HW  =   10      //hash width
    )(
    //system signals
    input                   clk,
    input                   rst_n,

    //RAM A
    input                   rama_wea_i,
    input   [HW-1:0]        rama_addra_i,
    input   [(DW+1)*SN-1:0] rama_dina_i,
    output  [(DW+1)*SN-1:0] rama_douta_o,

    input                   rama_web_i,
    input   [HW-1:0]        rama_addrb_i,
    input   [(DW+1)*SN-1:0] rama_dinb_i,
    output  [(DW+1)*SN-1:0] rama_doutb_o,

    //RAM B
    input                   ramb_wea_i,
    input   [HW-1:0]        ramb_addra_i,
    input   [(DW+1)*SN-1:0] ramb_dina_i,
    output  [(DW+1)*SN-1:0] ramb_douta_o,

    input                   ramb_web_i,
    input   [HW-1:0]        ramb_addrb_i,
    input   [(DW+1)*SN-1:0] ramb_dinb_i,
    output  [(DW+1)*SN-1:0] ramb_doutb_o
    );

    generate
        genvar i;
        for (i=0;i<SN;i=i+1) begin:gen_ram
            dual_port_ram #(
                .DPW        (HW),
                .DW         (DW+1)
            ) U0_dual_port_ram(
                //port a
                .clka       (clk),
                .wea        (rama_wea_i),
                .addra      (rama_addra_i),
                .dina       (rama_dina_i[(DW+1)*(i+1)-1:(DW+1)*i]),
                .douta      (rama_douta_o[(DW+1)*(i+1)-1:(DW+1)*i]),

                //port b
                .clkb       (clk),
                .web        (rama_web_i),
                .addrb      (rama_addrb_i),
                .dinb       (rama_dinb_i[(DW+1)*(i+1)-1:(DW+1)*i]),
                .doutb      (rama_doutb_o[(DW+1)*(i+1)-1:(DW+1)*i])
            );

            dual_port_ram #(
                .DPW        (HW),
                .DW         (DW+1)
            ) U1_dual_port_ram(
                //port a
                .clka       (clk),
                .wea        (ramb_wea_i),
                .addra      (ramb_addra_i),
                .dina       (ramb_dina_i[(DW+1)*(i+1)-1:(DW+1)*i]),
                .douta      (ramb_douta_o[(DW+1)*(i+1)-1:(DW+1)*i]),

                //port b
                .clkb       (clk),
                .web        (ramb_web_i),
                .addrb      (ramb_addrb_i),
                .dinb       (ramb_dinb_i[(DW+1)*(i+1)-1:(DW+1)*i]),
                .doutb      (ramb_doutb_o[(DW+1)*(i+1)-1:(DW+1)*i])
            );
        end
    endgenerate

endmodule
