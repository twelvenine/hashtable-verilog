`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: tocanemis
// 
// Create Date: 2022/03/19 15:28:15
// Design Name: 
// Module Name: easy_hash_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

(* dont_touch="true" *)

module easy_hash_top#(
    parameter               SN  =   1,      //slot number
                            HW  =   10,     //hash width
                            DW  =   32      //data width
    )(
    //system signals
    input                   clk,
    input                   rst_n,

    //insert
    input                   insert_i,
    input   [DW-1:0]        insert_data_i,
    output                  insert_error_o,
    output                  insert_end_o,

    //delete
    input                   delete_i,
    input   [DW-1:0]        delete_data_i,
    output                  delete_error_o,
    output                  delete_end_o,

    //search a
    input                   search_a_i,
    input   [DW-1:0]        search_a_data_i,
    output                  search_a_exist_o,
    output                  search_a_end_o,

    //search b
    input                   search_b_i,
    input   [DW-1:0]        search_b_data_i,
    output                  search_b_exist_o,
    output                  search_b_end_o
    );

    //ram
    wire                    ram_wea;
    wire    [HW-1:0]        ram_addra;
    wire    [(DW+1)*SN-1:0] ram_dina;
    wire    [(DW+1)*SN-1:0] ram_douta;

    //side B
    wire                    ram_web;
    wire    [HW-1:0]        ram_addrb;
    wire    [(DW+1)*SN-1:0] ram_dinb;
    wire    [(DW+1)*SN-1:0] ram_doutb;

    //easy hash
    easy_hash #(
        .SN                 (SN),   //slot number
        .HW                 (HW),   //hash width
        .DW                 (DW)    //data width
    ) U_easy_hash(
        //system signals
        .clk                (clk),
        .rst_n              (rst_n),

        //insert
        .insert_i           (insert_i),
        .insert_data_i      (insert_data_i),
        .insert_error_o     (insert_error_o),
        .insert_end_o       (insert_end_o),

        //delete
        .delete_i           (delete_i),
        .delete_data_i      (delete_data_i),
        .delete_error_o     (delete_error_o),
        .delete_end_o       (delete_end_o),

        //search port-a
        .search_a_i         (search_a_i),
        .search_a_data_i    (search_a_data_i),
        .search_a_exist_o   (search_a_exist_o),
        .search_a_end_o     (search_a_end_o),

        //search port-b
        .search_b_i         (search_b_i),
        .search_b_data_i    (search_b_data_i),
        .search_b_exist_o   (search_b_exist_o),
        .search_b_end_o     (search_b_end_o),

        //RAM interface
        //side A
        .ram_wea_o          (ram_wea),
        .ram_addra_o        (ram_addra),
        .ram_dina_o         (ram_dina),
        .ram_douta_i        (ram_douta),

        //side B
        .ram_web_o          (ram_web),
        .ram_addrb_o        (ram_addrb),
        .ram_dinb_o         (ram_dinb),
        .ram_doutb_i        (ram_doutb)
    );

    //ram
    easy_hash_ram_top #(
        .SN                 (SN),   //slot number
        .DW                 (DW),   //data width
        .HW                 (HW)    //hash width
    ) U_easy_hash_ram_top(
        //system signals
        .clk                (clk),
        .rst_n              (rst_n),

        //ram port a
        .ram_wea_i          (ram_wea),
        .ram_addra_i        (ram_addra),
        .ram_dina_i         (ram_dina),
        .ram_douta_o        (ram_douta),

        //ram port b
        .ram_web_i          (ram_web),
        .ram_addrb_i        (ram_addrb),
        .ram_dinb_i         (ram_dinb),
        .ram_doutb_o        (ram_doutb)
    );

endmodule
