`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: tocanemis
// 
// Create Date: 2022/03/19 19:29:30
// Design Name: 
// Module Name: easy_hash_ram_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

(* dont_touch="true" *)

module easy_hash_ram_top#(
    parameter               SN  =   1,      //slot number
                            DW  =   32,     //data width
                            HW  =   10      //hash width
    )(
    //system signals
    input                   clk,
    input                   rst_n,

    //ram port a
    input                   ram_wea_i,
    input   [HW-1:0]        ram_addra_i,
    input   [(DW+1)*SN-1:0] ram_dina_i,
    output  [(DW+1)*SN-1:0] ram_douta_o,

    //ram port b
    input                   ram_web_i,
    input   [HW-1:0]        ram_addrb_i,
    input   [(DW+1)*SN-1:0] ram_dinb_i,
    output  [(DW+1)*SN-1:0] ram_doutb_o
    );

    generate
        genvar i;
        for (i=0;i<SN;i=i+1) begin:gen_ram
            dual_port_ram #(
                .DPW        (HW),
                .DW         (DW+1)
            ) U_dual_port_ram(
                //port a
                .clka       (clk),
                .wea        (ram_wea_i),
                .addra      (ram_addra_i),
                .dina       (ram_dina_i[(DW+1)*(i+1)-1:(DW+1)*i]),
                .douta      (ram_douta_o[(DW+1)*(i+1)-1:(DW+1)*i]),

                //port b
                .clkb       (clk),
                .web        (ram_web_i),
                .addrb      (ram_addrb_i),
                .dinb       (ram_dinb_i[(DW+1)*(i+1)-1:(DW+1)*i]),
                .doutb      (ram_doutb_o[(DW+1)*(i+1)-1:(DW+1)*i])
            );
        end
    endgenerate

endmodule
