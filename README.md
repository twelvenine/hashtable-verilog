# hashtable-verilog

### 1.介绍
哈希表的三种FPGA实现结构:
1. 简单哈希表
2. 基于多哈希的哈希表
3. 基于布谷鸟哈希的哈希表

三种哈希表可支持操作:
- [x] 插入
- [x] 删除
- [x] 查询

### 2.设计说明文档
1. 哈希表简单介绍：https://zhuanlan.zhihu.com/p/481375940
2. 简单哈希和多哈希设计：https://zhuanlan.zhihu.com/p/481379335
3. 布谷鸟哈希设计：https://zhuanlan.zhihu.com/p/482034449
4. 性能测试与比较：https://zhuanlan.zhihu.com/p/481380456

### 3.RTL结构
---**crc**

+-----CRC32_D32.v

---**cuckoo_hash**

+-----cuckoo_hash.v

+-----cuckoo_hash_ram_top.v

+-----cuckoo_hash_top.v

---**easy_hash**

+-----easy_hash.v

+-----easy_hash_ram_top.v

+-----easy_hash_top.v

---**multi_hash**

+-----multi_hash.v

+-----multi_hash_ram_top.v

+-----multi_hash_top.v

---**ram**

+-----dual_port_ram.v

+-----syn_fifo_fwft.v

### 4.接口说明

```

//system signals
clk,                //全局时钟
rst_n,              //低电平复位

//insert
insert_i,           //插入信号
insert_data_i,      //插入数据
insert_error_o,     //插入错误指示
insert_end_o,       //插入结束信号

//delete
delete_i,           //删除信号
delete_data_i,      //删除数据
delete_error_o,     //删除错误指示
delete_end_o,       //删除结束信号

//search Port-A
search_a_i,         //A口查询信号
search_a_data_i,    //A口查询数据
search_a_exist_o,   //A口查询错误指示
search_a_end_o,     //A口查询结束信号

//search Port-B
search_b_i,         //B口查询信号
search_b_data_i,    //B口查询数据
search_b_exist_o,   //B口查询错误指示
search_b_end_o      //B口查询结束信号

```

### 5.其他

对上述内容有任何疑问、意见和建议，可在下面评论，此外，还可以知乎私信@十二点过九分或者邮箱联系：twelvenine@foxmail.com